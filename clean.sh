#!/bin/bash

rm -rf build
rm -rf Makefile
rm -rf docs/*
rm -f src/*TestRunner*
rm -f src/*.o src/*.mod
rm -f src/funit.tmp src/*_fun.f90
rm -f CMakeCache.txt
rm -f cmake_install.cmake src/cmake_install.cmake
rm -rf CMakeFiles src/CMakeFiles src/examples/CMakeFiles src/tests/CMakeFiles
