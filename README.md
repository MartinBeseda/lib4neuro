## Dependencies
- [Git](https://git-scm.com/)
- [cmake](https://cmake.org/) (version >= 3.0)
- [Boost](https://www.boost.org/)
- [exprtk](http://www.partow.net/programming/exprtk/)

#### Linux
- `make`
- C++ compiler

#### Windows
- [Visual Studio](https://visualstudio.microsoft.com/) (install together with `cl` and `SDK`!)

## Compilation
- Clone the repository to your PC

    ```
    git clone git@bitbucket.org:MartinBeseda/lib4neuro.git
    ```
    
- Clone the repository of `exprtk`

    ```
    git clone https://github.com/ArashPartow/exprtk.git
    ```

#### Linux (Debian-based)
- Install package `libboost-all-dev`

    ```
    sudo apt-get install libboost-all-dev
    ```
    
- Set `EXPRTK_INCLUDE_DIR` environmental variable to the path of your `exprtk` folder
    
    ``` 
    export EXPRTK_INCLUDE_DIR="path to the exprtk folder"
    ```
    
- Go to the `lib4Neuro` folder

- In the file `build.sh`  set correctly variables `BUILD_TYPE` and `CXX_COMPILER`

- Run

    ```
    ./build.sh
    ```

#### Windows
- Download `Boost` from [this link](https://www.boost.org/users/download/) and extract the archive
- Run the Visual Studio Command Prompt (Start Menu -> Visual Studio *** -> Developer Command Prompt)
  and go to the folder with `Boost`

    ```
    cd "folder with Boost"
    bootstrap.bat
    b2.exe --build-type=complete
    ```
- Set `BOOST_ROOT` environmental variable to the path of your folder containing `Boost`

        1. In Search, search for and then select: System (Control Panel)
        2. Click the Advanced system settings link.
        3. Click Environment Variables.
        4. In the New System Variable window, specify the value of the `BOOST_ROOT` environment variable

- Set `EXPRTK_INCLUDE_DIR` environmental variable to the path of your `exprtk` folder

- Go to the `lib4neuro` folder

- In the file `build.bat`  set correctly variables `BUILD_TYPE`, `CXX_COMPILER` and `C_COMPILER`

- Run

    ```
    build.bat
    ```

