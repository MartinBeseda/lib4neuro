//
// Created by martin on 7/16/18.
//

#ifndef INC_4NEURO_4NEURO_H
#define INC_4NEURO_4NEURO_H

//TODO make only public interface visible

#include "../src/DataSet/DataSet.h"
#include "../src/ErrorFunction/ErrorFunctions.h"
#include "../src/LearningMethods/ParticleSwarm.h"
#include "../src/NetConnection/ConnectionFunctionGeneral.h"
#include "../src/NetConnection/ConnectionFunctionIdentity.h"
#include "../src/Network/NeuralNetwork.h"
#include "../src/Network/NeuralNetworkSum.h"
#include "../src/Neuron/Neuron.h"
#include "../src/Neuron/NeuronBinary.h"
#include "../src/Neuron/NeuronLinear.h"
#include "../src/Neuron/NeuronLogistic.h"

#endif //INC_4NEURO_4NEURO_H
