@echo off
title Build lib4neuro project

rem Build type (Release/Debug)
set BUILD_TYPE=Debug

rem C++ compiler
set CXX_COMPILER=cl
set C_COMPILER=cl

rem Makefile generator
rem For the complete list type "cmake --help"
rem Example: "MSYS Makefiles", "MinGW Makefiles", "NMake Makefiles"
set MAKEFILE_GENERATOR="Visual Studio 15 2017"

call clean.bat
cmake -G %MAKEFILE_GENERATOR% -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=%BUILD_TYPE% -DCMAKE_CXX_COMPILER=%CXX_COMPILER% -DCMAKE_C_COMPILER=%C_COMPILER% -DBOOST_ROOT=%BOOST_ROOT% -DBOOST_LIBRARYDIR=%BOOST_LIBRARYDIR% -DBOOST_INCLUDEDIR=%BOOST_INCLUDEDIR% -DBoost_DEBUG:BOOL=ON . 
cmake --build . --config %BUILD_TYPE% && (echo (Build complete.); echo (For examples have a look at the folder build/bin/examples.)) || (echo "Build finished with errors!")
