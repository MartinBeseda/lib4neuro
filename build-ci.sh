#!/bin/sh

#------------#------------------------------------------------------------
# Parameters #
#------------#

# Build type (Release/Debug)
BUILD_TYPE=Debug

# C++ compiler
CXX_COMPILER="g++-8"

if [ -z "$BUILD_TYPE" ] || [ -z "$CXX_COMPILER" ]; then
    (>&2 echo "Set, please, both BUILD_TYPE and CXX_COMPILER variables in the 'build.sh' script.")
    exit 2
fi

$(pwd)/clean.sh
cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DCMAKE_CXX_COMPILER=${CXX_COMPILER} .
cmake --build . --config ${BUILD_TYPE} -- -j${N_CORES} && (tput setaf 2; echo "Build complete."; echo "For examples have a look at the folder build/bin/examples."; tput sgr 0; ) || (tput setaf 1; echo "Build finished with errors!"; tput sgr 0; exit 1;)
