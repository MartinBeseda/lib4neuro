/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 14.6.18 -
 */

#include "ConnectionFunctionIdentity.h"

ConnectionFunctionIdentity::ConnectionFunctionIdentity( ) {
//    this->type = CONNECTION_TYPE::IDENTITY;
    this->is_unitary = true;
}

ConnectionFunctionIdentity::ConnectionFunctionIdentity( size_t pidx ) {
//    this->type = CONNECTION_TYPE::IDENTITY;
    this->param_idx = pidx;
    this->is_unitary = false;
}

double ConnectionFunctionIdentity::eval( std::vector<double> &parameter_space ) {

    if( this->is_unitary ){
        return 1.0;
    }

    return parameter_space.at(this->param_idx);
}

void ConnectionFunctionIdentity::eval_partial_derivative(std::vector<double> &parameter_space, std::vector<double> &weight_gradient, double alpha) {

    if( this->is_unitary ){
        return;
    }

    weight_gradient[this->param_idx] += alpha;
}