/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 14.6.18 -
 */

#ifndef INC_4NEURO_CONNECTIONWEIGHTIDENTITY_H
#define INC_4NEURO_CONNECTIONWEIGHTIDENTITY_H

#include "ConnectionFunctionGeneral.h"

class ConnectionFunctionGeneral;

/**
 *
 */
class ConnectionFunctionIdentity:public ConnectionFunctionGeneral {
    friend class boost::serialization::access;
    friend class NeuralNetwork;

private:

    size_t param_idx = 0;

    bool is_unitary = false;

protected:
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & boost::serialization::base_object<ConnectionFunctionGeneral>(*this);
        ar & this->param_idx;
        ar & this->is_unitary;
    };

public:

    /**
     *
     */
    ConnectionFunctionIdentity( );

    /**
     *
     */
    ConnectionFunctionIdentity( size_t pidx );

    /**
     *
     * @return
     */
    double eval( std::vector<double> &parameter_space ) override;

    /**
     *
     * @param weight_gradient
     * @param alpha
     */
    void eval_partial_derivative(std::vector<double> &parameter_space, std::vector<double> &weight_gradient, double alpha) override;
};


#endif //INC_4NEURO_CONNECTIONWEIGHTIDENTITY_H
