/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 14.6.18 -
 */

#include "ConnectionFunctionGeneral.h"

ConnectionFunctionGeneral::ConnectionFunctionGeneral() {}

ConnectionFunctionGeneral::ConnectionFunctionGeneral(std::vector<size_t > &param_indices, std::string &function_string) {
    this->param_indices = param_indices;
}

ConnectionFunctionGeneral::~ConnectionFunctionGeneral() {

}

double ConnectionFunctionGeneral::eval( std::vector<double> &parameter_space ) {
    //TODO

    return 0.0;
}

void ConnectionFunctionGeneral::eval_partial_derivative(std::vector<double> &parameter_space, std::vector<double> &weight_gradient, double alpha) {
    //TODO
}
