/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 14.6.18 -
 */

#ifndef INC_4NEURO_CONNECTIONWEIGHT_H
#define INC_4NEURO_CONNECTIONWEIGHT_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/vector.hpp>
#include <functional>
#include <vector>

class ConnectionFunctionGeneral {
private:
    friend class boost::serialization::access;

    template <class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & this->param_indices;
    };

protected:

    /**
     *
     */
    std::vector<size_t> param_indices;

public:

    /**
     *
     */
    ConnectionFunctionGeneral();

    /**
     *
     * @param param_count
     * @param f
     */
    ConnectionFunctionGeneral(std::vector<size_t> &param_indices, std::string &function_string);

    /**
     *
     */
    virtual ~ConnectionFunctionGeneral( );


    /**
     *
     * @return
     */
    virtual double eval( std::vector<double> &parameter_space );

    /**
     * Performs partial derivative of this transfer function according to all parameters. Adds the values multiplied
     * by alpha to the corresponding gradient vector
     */
    virtual void eval_partial_derivative( std::vector<double> &parameter_space, std::vector<double> &weight_gradient, double alpha );

};


#endif //INC_4NEURO_CONNECTIONWEIGHT_H
