/**
 * DESCRIPTION OF THE CLASS
 *
 * @author Martin Beseda
 * @author Martin Mrovec
 * @author Michal Kravčenko
 * @date 2017 - 2018
 */
//TODO  correct docs in this and all child classes
 #ifndef NEURON_H_
 #define NEURON_H_

#include <boost/serialization/base_object.hpp>
#include <vector>

class IDifferentiable;

/**
  * Abstract class representing a general neuron
  */
class Neuron {
private:
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){};

public:

    /**
     * Destructor of the Neuron object
     * this level deallocates the array 'activation_function_parameters'
     * also deallocates the OUTGOING connections
     */
    virtual ~Neuron();

    /**
     * Performs the activation function and returns the result
     */
    virtual double activate( double x, double b ) = 0;

}; /* end of Neuron class */


/**
 * Class serving as an interface providing 'activation_function_eval_partial_derivative',
 * 'activation_function_eval_derivative',  'get_partial_derivative' and
 * 'get_derivative' methods.
 */
class IDifferentiable {

    /**
     * Calculates the derivative with respect to the argument, ie the 'potential'
     * @return f'(x), where 'f(x)' is the activation function and 'x' = 'potential'
     */
    virtual double activation_function_eval_derivative( double x, double b ) = 0;

    /**
     * Calculates the derivative with respect to the bias
     * @return d/db f'(x), where 'f(x)' is the activation function, 'x' is the 'potential'
     * and 'b' is the bias
     */
    virtual double activation_function_eval_derivative_bias( double x, double b ) = 0;

    /**
     * Returns a Neuron pointer object with activation function being the partial derivative of
     * the activation function of this Neuron object with respect to the argument, i.e. 'potential'
     * @return
     */
    virtual Neuron* get_derivative( ) = 0;

}; /* end of IDifferentiable class */

 #endif /* NEURON_H_ */