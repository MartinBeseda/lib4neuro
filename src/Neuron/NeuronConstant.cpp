/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 8.8.18 -
 */

#include "NeuronConstant.h"

NeuronConstant::NeuronConstant( double c ) {
    this->p = c;
}

double NeuronConstant::activate( double x, double b ) {
    return  this->p;
}

double NeuronConstant::activation_function_eval_derivative_bias( double x, double b ) {
    return 0.0;
}

double NeuronConstant::activation_function_eval_derivative( double x, double b ) {
    return 0.0;
}

Neuron* NeuronConstant::get_derivative() {
    NeuronConstant* output = new NeuronConstant( );
    return output;
}