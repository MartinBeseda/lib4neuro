/**
 * DESCRIPTION OF THE CLASS
 *
 * @author Martin Beseda
 * @author Martin Mrovec
 * @author Michal Kravčenko
 * @date 2017 - 2018
 */

#ifndef INC_4NEURO_NEURONLOGISTIC_H
#define INC_4NEURO_NEURONLOGISTIC_H

#include <cmath>
#include "Neuron.h"
#include "../constants.h"


class NeuronLogistic:public Neuron, public IDifferentiable {
    friend class boost::serialization::access;

protected:
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        //TODO separate implementation to NeuronLogistic.cpp!
        ar & boost::serialization::base_object<Neuron>(*this);
    };

public:
    /**
     * Constructs the object of the Logistic neuron with activation function
     * f(x) = (1 + e^(-x + b))^(-1)
     */
    explicit NeuronLogistic( );

    /**
     * Evaluates '(1 + e^(-x + b))^(-1)' and stores the result into the 'state' property
     */
    virtual double activate( double x, double b ) override;

    /**
     * Calculates the partial derivative of the activation function
     * f(x) = (1 + e^(-x + b))^(-1)
     * @return Partial derivative of the activation function according to the
     * bias, returns: -e^(b - x)/(e^(b - x) + 1)^2
     */
    virtual double activation_function_eval_derivative_bias( double x, double b ) override;
    /**
     * Calculates d/dx of (1 + e^(-x + b))^(-1)
     * @return e^(b - x)/(e^(b - x) + 1)^2
     */
    virtual double activation_function_eval_derivative( double x, double b ) override;

    /**
     * Returns a pointer to a Neuron with derivative as its activation function
     * @return
     */
    virtual NeuronLogistic* get_derivative( ) override;
};


class NeuronLogistic_d1:public NeuronLogistic {
private:
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        //TODO separate implementation to Neuronogistic_d1.cpp!
        ar & boost::serialization::base_object<Neuron>(*this);
    };

public:

    /**
     * Constructs the object of the Logistic neuron with activation function
     * f(x) = e^(b - x)/(e^(b - x) + 1)^2
     * @param[in] b Bias
     */
    explicit NeuronLogistic_d1( );

    /**
     * Evaluates 'e^(b - x)/(e^(b - x) + 1)^2' and returns the result
     */
    virtual double activate( double x, double b ) override;

    /**
     * Calculates the partial derivative of the activation function
     * f(x) = e^(b - x)/(e^(b - x) + 1)^2
     * @return Partial derivative of the activation function according to the
     * bias, returns: (e^(b + x) (e^x - e^b))/(e^b + e^x)^3
     */
    virtual double activation_function_eval_derivative_bias( double x, double b ) override;

    /**
     * Calculates d/dx of  e^(b - x)*(1 + e^(b - x))^(-2)
     * @return  (e^(b + x) (e^b - e^x))/(e^b + e^x)^3
     */
    virtual double activation_function_eval_derivative( double x, double b ) override;

    /**
     * Returns a pointer to a Neuron with derivative as its activation function
     * @return
     */
    virtual NeuronLogistic* get_derivative( ) override;
};





class NeuronLogistic_d2:public NeuronLogistic_d1 {
private:
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        //TODO separate implementation to NeuronLogistic_d1.cpp!
        ar & boost::serialization::base_object<Neuron>(*this);
    };

public:

    /**
     * Constructs the object of the Logistic neuron with activation function
     * f(x) = (e^(b + x) (e^b - e^x))/(e^b + e^x)^3
     */
    explicit NeuronLogistic_d2( );

    /**
     * Evaluates '(e^(b + x) (e^b - e^x))/(e^b + e^x)^3' and returns the result
     */
    virtual double activate( double x, double b ) override;

    /**
     * Calculates the partial derivative of the activation function
     * f(x) = (e^(b + x) (e^b - e^x))/(e^b + e^x)^3
     * @return Partial derivative of the activation function according to the
     * bias, returns: -(e^(b + x) (-4 e^(b + x) + e^(2 b) + e^(2 x)))/(e^b + e^x)^4
     */
    virtual double activation_function_eval_derivative_bias( double x, double b ) override;

    /**
     * Calculates d/dx of  (e^(b + x) (e^b - e^x))/(e^b + e^x)^3
     * @return (e^(b + x) (-4 e^(b + x) + e^(2 b) + e^(2 x)))/(e^b + e^x)^4
     */
    virtual double activation_function_eval_derivative( double x, double b ) override;

    /**
     *
     * @return
     */
    virtual NeuronLogistic* get_derivative( ) override;

};



#endif //INC_4NEURO_NEURONLOGISTIC_H
