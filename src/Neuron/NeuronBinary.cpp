//
// Created by fluffymoo on 11.6.18.
//

#include "NeuronBinary.h"

NeuronBinary::NeuronBinary( ) {}

double NeuronBinary::activate( double x, double b ) {

    if(x >= b){
        return 1.0;
    }
    else{
        return 0.0;
    }
}
