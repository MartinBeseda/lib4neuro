/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 8.8.18 -
 */

#ifndef INC_4NEURO_NEURONCONSTANT_H
#define INC_4NEURO_NEURONCONSTANT_H


#include "Neuron.h"

class NeuronConstant: public Neuron, public IDifferentiable {
private:
    friend class boost::serialization::access;

    double p = 0.0;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & boost::serialization::base_object<Neuron>(*this);

        ar & this->p;
    };

public:

    /**
     * Constructs the object of the Linear neuron with activation function
     * f(x) = c
     * @param[in] c Constant value
     */
    explicit NeuronConstant( double c = 0.0 );

    /**
     * Evaluates and returns 'c'
     */
    double activate( double x, double b ) override;

    /**
     * Calculates the partial derivative of the activation function
     * f(x) = c at point x
     * @return Partial derivative of the activation function according to the
     * 'bias' parameter. Returns 0.0
     */
    double activation_function_eval_derivative_bias( double x, double b ) override;

    /**
     * Calculates d/dx of (c) at point x
     * @return 0.0
     */
    double activation_function_eval_derivative( double x, double b ) override;

    /**
     * Returns a pointer to a Neuron with derivative as its activation function
     * @return
     */
    Neuron* get_derivative( ) override;
};

#endif //INC_4NEURO_NEURONCONSTANT_H
