/**
 * DESCRIPTION OF THE CLASS
 *
 * @author Martin Beseda
 * @author Martin Mrovec
 * @author Michal Kravčenko
 * @date 2017 - 2018
 */

#ifndef INC_4NEURO_NEURONLINEAR_H
#define INC_4NEURO_NEURONLINEAR_H

#include "Neuron.h"
#include "NeuronConstant.h"
#include <boost/serialization/base_object.hpp>


/**
 * Linear neuron class - uses activation function in the form f(x)=a*x + b,
 * 'x' being the neuron's potential
 */
class NeuronLinear:public Neuron, public IDifferentiable {
private:
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & boost::serialization::base_object<Neuron>(*this);
    };

public:

    /**
     * Constructs the object of the Linear neuron with activation function
     * f(x) = x + b
     * @param[in] b Bias
     */
    explicit NeuronLinear( );

    /**
     * Evaluates 'x + b' and stores the result into the 'state' property
     */
    double activate( double x, double b ) override;

    /**
     * Calculates the partial derivative of the activation function
     * f(x) = x + b at point x
     * @return Partial derivative of the activation function according to the
     * 'bias' parameter. Returns 1.0
     */
    double activation_function_eval_derivative_bias( double x, double b ) override;

    /**
     * Calculates d/dx of (x + b) at point x
     * @return 1.0
     */
    double activation_function_eval_derivative( double x, double b ) override;

    /**
     * Returns a pointer to a Neuron with derivative as its activation function
     * @return
     */
    Neuron* get_derivative( ) override;

};


#endif //INC_4NEURO_NEURONLINEAR_H
