/**
 * DESCRIPTION OF THE CLASS
 *
 * @author Martin Beseda
 * @author Martin Mrovec
 * @author Michal Kravčenko
 * @date 2017 - 2018
 */

#ifndef INC_4NEURO_NEURONBINARY_H
#define INC_4NEURO_NEURONBINARY_H

#include "Neuron.h"

/**
 *  Binary neuron class - uses unit-step as the activation function
 */
class NeuronBinary:public Neuron {
private:
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & boost::serialization::base_object<Neuron>(*this);
    };

public:

    /**
     * Default constructor for the binary Neuron
     * @param[in] threshold Denotes when the neuron is activated
     * When neuron potential exceeds 'threshold' value it becomes excited
     */
    explicit NeuronBinary( );

    /**
     * Performs the activation function and stores the result into the 'state' property
     */
    double activate( double x, double b ) override;

};

#endif //INC_4NEURO_NEURONBINARY_H
