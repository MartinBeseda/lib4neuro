//
// Created by fluffymoo on 11.6.18.
//

#include "NeuronLinear.h"



NeuronLinear::NeuronLinear( ) {}

double NeuronLinear::activate( double x, double b ) {

    return  x + b;
}

double NeuronLinear::activation_function_eval_derivative_bias( double x, double b ) {
    return 1.0;
}

double NeuronLinear::activation_function_eval_derivative( double x, double b ) {
    return 1.0;
}

Neuron* NeuronLinear::get_derivative() {
    NeuronConstant* output = new NeuronConstant( 1.0 );
    return output;
}

//template<class Archive>
//void NeuronLinear::serialize(Archive & ar, const unsigned int version) {
//    ar & boost::serialization::base_object<Neuron>(*this);
//}
