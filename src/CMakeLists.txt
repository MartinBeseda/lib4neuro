add_subdirectory(tests bin/unit-tests)
add_subdirectory(examples bin/examples)

add_library(4neuro SHARED
        Neuron/Neuron.cpp
        Neuron/NeuronBinary.cpp
        Neuron/NeuronConstant.cpp
        Neuron/NeuronLinear.cpp
        Neuron/NeuronLogistic.cpp
        Network/NeuralNetwork.cpp
        Network/NeuralNetworkSum.cpp
        NetConnection/ConnectionFunctionGeneral.cpp
        NetConnection/ConnectionFunctionIdentity.cpp
        LearningMethods/ParticleSwarm.cpp
        DataSet/DataSet.cpp
        ErrorFunction/ErrorFunctions.cpp
        Solvers/DESolver.cpp
        LearningMethods/ILearningMethods.h)


add_library(boost_unit_test SHARED boost_test_lib_dummy.cpp)
add_library(exprtk SHARED exprtk.cpp)

target_link_libraries(4neuro ${Boost_LIBRARIES} boost_unit_test exprtk)
