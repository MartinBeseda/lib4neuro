//
// Created by martin on 7/15/18.
//

#ifndef INC_4NEURO_ERRORFUNCTION_H
#define INC_4NEURO_ERRORFUNCTION_H

#include "../Network/NeuralNetwork.h"
#include "../DataSet/DataSet.h"

enum ErrorFunctionType{
    ErrorFuncMSE
};

class ErrorFunction {
public:

    /**
     *
     * @param weights
     * @return
     */
    virtual double eval(std::vector<double>* weights = nullptr) = 0;
    
    /**
     * 
     * @return 
     */
    virtual size_t get_dimension();

protected:

    /**
     *
     */
    size_t dimension = 0;
};

class MSE : public ErrorFunction {

public:
    /**
     * Constructor for single neural network
     * @param net
     * @param ds
     */
    MSE(NeuralNetwork* net, DataSet* ds);

    /**
     *
     * @param weights
     * @return
     */
    virtual double eval(std::vector<double>* weights = nullptr);

private:

    NeuralNetwork* net;
    DataSet* ds;
};

class ErrorSum : public ErrorFunction{
public:
    /**
     *
     */
    ErrorSum();

    /**
     *
     */
    ~ErrorSum();

    /**
     *
     * @param weights
     * @return
     */
    virtual double eval(std::vector<double>* weights = nullptr);

    /**
     *
     * @param F
     */
    void add_error_function(ErrorFunction *F, double alpha = 1.0);

    /**
     *
     * @return
     */
    size_t get_dimension() override;

private:
    std::vector<ErrorFunction*>* summand;
    std::vector<double> *summand_coefficient;
};


#endif //INC_4NEURO_ERRORFUNCTION_H
