/**
 * This file contains the header for the NeuralNetwork class representing a function in the form of a directed graph,
 * in which the vertices are called Neurons (with activation functions) and the edges Connections (with transfer functions)
 *
 * @author Michal Kravčenko
 * @date 13.6.18 -
 */

//TODO preprocess the feed-forward and backward passes for more efficient parallelism

#ifndef INC_4NEURO_NEURALNETWORK_H
#define INC_4NEURO_NEURALNETWORK_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <fstream>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>

#include "Neuron/Neuron.h"
#include "Neuron/NeuronConstant.h"
#include "Neuron/NeuronBinary.h"
#include "Neuron/NeuronLinear.h"
#include "Neuron/NeuronLogistic.h"
#include "NetConnection/ConnectionFunctionGeneral.h"
#include "NetConnection/ConnectionFunctionIdentity.h"
#include "settings.h"



enum class BIAS_TYPE{NEXT_BIAS, NO_BIAS, EXISTING_BIAS};

enum class SIMPLE_CONNECTION_TYPE{NEXT_WEIGHT, UNITARY_WEIGHT, EXISTING_WEIGHT};


/**
 *
 */
class NeuralNetwork {
private:
    friend class boost::serialization::access;

    /**
     *
     */
    std::vector<Neuron*> *neurons = nullptr;

    /**
     *
     */
    std::vector<size_t>* input_neuron_indices = nullptr;

    /**
     *
     */
    std::vector<size_t>* output_neuron_indices = nullptr;

    /**
     *
     */
    std::vector<double>* connection_weights = nullptr;

    /**
     *
     */
    std::vector<double>* neuron_biases = nullptr;

    /**
     *
     */
    std::vector<int>* neuron_bias_indices = nullptr;

    /**
     *
     */
    std::vector<double>* neuron_potentials = nullptr;

    /**
     *
     */
    std::vector<ConnectionFunctionGeneral*> * connection_list = nullptr;

    /**
     *
     */
    std::vector<std::vector<std::pair<size_t, size_t>>*> * inward_adjacency = nullptr;

    /**
     *
     */
    std::vector<std::vector<std::pair<size_t, size_t>>*> * outward_adjacency = nullptr;

    /**
     *
     */
    std::vector<std::vector<size_t>*> *neuron_layers_feedforward = nullptr;

    /**
     *
     */
    std::vector<std::vector<size_t>*> *neuron_layers_feedbackward = nullptr;

     /**
     *
     */
    bool layers_analyzed = false;

    /**
     *
     */
    bool delete_weights = true;

    /**
     *
     */
    bool delete_biases = true;

    /**
     * Adds a new connection to the local list of connections
     * @param con Connection object to be added
     * @return Returns the index of the added connection among all the connections
     */
    size_t add_new_connection_to_list(ConnectionFunctionGeneral* con);

    /**
     * Adds a new entry (oriented edge s -> t) to the adjacency list of this network
     * @param s Index of the source neuron
     * @param t Index of the target neuron
     * @param con_idx Index of the connection representing the edge
     */
    void add_outward_connection(size_t s, size_t t, size_t con_idx);

    /**
     * Adds a new entry (oriented edge s <- t) to the adjacency list of this network
     * @param s Index of the source neuron
     * @param t Index of the target neuron
     * @param con_idx Index of the connection representing the edge
     */
    void add_inward_connection(size_t s, size_t t, size_t con_idx);

    /**
     * Performs one feedforward pass and feedbackward pass during which determines the layers of this neural network
     * for simpler use during evaluation and learning
     */
    void analyze_layer_structure( );

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & this->neurons;
        ar & this->input_neuron_indices;
        ar & this->output_neuron_indices;
        ar & this->connection_list;
        ar & this->neuron_biases;
        ar & this-> neuron_bias_indices;
        ar & this->neuron_potentials;
        ar & this->connection_weights;
        ar & this->inward_adjacency;
        ar & this->outward_adjacency;
        ar & this->neuron_layers_feedforward;
        ar & this->neuron_layers_feedbackward;
        ar & this->layers_analyzed;
        ar & this->delete_weights;
        ar & this->delete_biases;
    };

public:

    /**
     *
     */
    explicit NeuralNetwork();

    /**
     *
     */
    explicit NeuralNetwork(std::string filepath);

    /**
     *
     */
    virtual ~NeuralNetwork();

    /**
     * If possible, returns a neural net with 'input_neuron_indices' neurons as inputs and 'output_neuron_indices' as
     * outputs, otherwise returns nullptr. The returned object shares adjustable weights with this network. All
     * neurons are coppied (new instances), edges also. Uses a breadth-first search as the underlying algorithm.
     * @param input_neuron_indices
     * @param output_neuron_indices
     * @return
     */
    NeuralNetwork* get_subnet(std::vector<size_t> &input_neuron_indices, std::vector<size_t> &output_neuron_indices);

    /**
     * Replaces the values in @{this->connection_weights} and @{this->neuron_biases} by the provided values
     * @param parameters
     */
    virtual void copy_parameter_space(std::vector<double> *parameters);

    /**
     * Copies the pointers @{this->connection_weights} and @{this->neuron_biases} from the parental network, sets
     * flags to not delete the vectors in this object
     * @param parent_network
     */
    virtual void set_parameter_space_pointers( NeuralNetwork &parent_network );

    /**
     *
     * @param input
     * @param output
     * @param custom_weights_and_biases
     */
    virtual void eval_single(std::vector<double> &input, std::vector<double> &output, std::vector<double> *custom_weights_and_biases = nullptr);

    /**
     * Adds a new neuron to the list of neurons. Also assigns a valid bias value to its activation function
     * @param[in] n
     * @return
     */
    size_t add_neuron(Neuron* n, BIAS_TYPE bt = BIAS_TYPE::NEXT_BIAS, size_t bias_idx = 0);

    /**
     *
     * @param n1_idx
     * @param n2_idx
     * @return
     */
    size_t add_connection_simple(size_t n1_idx, size_t n2_idx, SIMPLE_CONNECTION_TYPE sct = SIMPLE_CONNECTION_TYPE::NEXT_WEIGHT, size_t weight_idx = 0 );

    /**
     * Take the existing connection with index 'connection_idx' in 'parent_network' and adds it to the structure of this
     * object
     * @param n1_idx
     * @param n2_idx
     * @param connection_idx
     * @param parent_network
     */
    void add_existing_connection(size_t n1_idx, size_t n2_idx, size_t connection_idx, NeuralNetwork &parent_network );


    /**
     *
     */
    void randomize_weights();

    /**
     *
     */
    void randomize_biases();

    /**
     *
     * @return
     */
    virtual size_t get_n_inputs();

    /**
     *
     * @return
     */
    virtual size_t get_n_outputs();

    /**
     *
     * @return
     */
    virtual size_t get_n_weights();

    /**
     *
     * @return
     */
    virtual size_t get_n_biases();

    /**
     *
     * @return
     */
    virtual int get_neuron_bias_index( size_t neuron_idx );

    /**
     *
     * @return
     */
    virtual size_t get_n_neurons();

    /**
     *
     * @param input_neurons_indices
     */
    void specify_input_neurons(std::vector<size_t> &input_neurons_indices);

    /**
     *
     * @param output_neurons_indices
     */
    void specify_output_neurons(std::vector<size_t> &output_neurons_indices);

    /**
     *
     */
    void print_weights();

    /**
     *
     */
    void print_stats();

    /**
     *
     * @return
     */
    std::vector<double>* get_parameter_ptr_weights();

    /**
     *
     * @return
     */
    std::vector<double>* get_parameter_ptr_biases();

    /**
     *
     * @param filepath
     */
    void save_text(std::string filepath);

};

#endif //INC_4NEURO_NEURALNETWORK_H
