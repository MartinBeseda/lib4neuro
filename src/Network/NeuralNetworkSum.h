/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 18.7.18 -
 */

#ifndef INC_4NEURO_NEURALNETWORKSUM_H
#define INC_4NEURO_NEURALNETWORKSUM_H

#include "NeuralNetwork.h"

class NeuralNetworkSum : public NeuralNetwork {
private:
    friend class boost::serialization::access;

    std::vector<NeuralNetwork*> * summand;
    std::vector<double> * summand_coefficient;

    template <class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & boost::serialization::base_object<NeuralNetwork>(*this);
        ar & this->summand;
        ar & this->summand_coefficient;
    };

public:
    NeuralNetworkSum( );
    virtual ~NeuralNetworkSum( );

    void add_network( NeuralNetwork *net, double alpha = 1.0 );

    virtual void eval_single(std::vector<double> &input, std::vector<double> &output, std::vector<double> *custom_weights_and_biases = nullptr);

    /**
     *
     * @return
     */
    virtual size_t get_n_inputs() override;

    /**
     *
     * @return
     */
    size_t get_n_outputs() override;

    /**
     *
     * @return
     */
    virtual size_t get_n_weights() override;

    /**
     *
     * @return
     */
    virtual size_t get_n_biases() override;

    /**
     *
     * @return
     */
    virtual size_t get_n_neurons() override;
};


#endif //INC_4NEURO_NEURALNETWORKSUM_H
