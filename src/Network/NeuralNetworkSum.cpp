/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 18.7.18 -
 */

#include "NeuralNetworkSum.h"

NeuralNetworkSum::NeuralNetworkSum(){
    this->summand = nullptr;
    this->summand_coefficient = nullptr;
}

NeuralNetworkSum::~NeuralNetworkSum() {
    if( this->summand ){
        delete this->summand;
    }
    if( this->summand_coefficient ){
        delete this->summand_coefficient;
    }
}

void NeuralNetworkSum::add_network(NeuralNetwork *net, double alpha) {
    if(!this->summand){
        this->summand = new std::vector<NeuralNetwork*>(0);
    }
    this->summand->push_back( net );

    if(!this->summand_coefficient){
        this->summand_coefficient = new std::vector<double>(0);
    }
    this->summand_coefficient->push_back( alpha );
}

void NeuralNetworkSum::eval_single(std::vector<double> &input, std::vector<double> &output, std::vector<double> *custom_weights_and_biases) {
    std::vector<double> mem_output(output.size());
    std::fill(output.begin(), output.end(), 0.0);

    for(size_t ni = 0; ni < this->summand->size(); ++ni){
        this->summand->at(ni)->eval_single(input, mem_output, custom_weights_and_biases);
        double alpha = this->summand_coefficient->at(ni);
        for(size_t j = 0; j < output.size(); ++j){
            output[j] += mem_output[j] * alpha;
        }
    }

}

size_t NeuralNetworkSum::get_n_weights(){
    //TODO insufficient solution, assumes the networks share weights
    if(this->summand){
        return this->summand->at(0)->get_n_weights();
    }

    return 0;
}

size_t NeuralNetworkSum::get_n_biases(){
    //TODO insufficient solution, assumes the networks share weights
    if(this->summand){
        return this->summand->at(0)->get_n_biases();
    }

    return 0;
}

size_t NeuralNetworkSum::get_n_inputs() {
    //TODO insufficient solution, assumes the networks share weights
    if(this->summand){
        return this->summand->at(0)->get_n_inputs();
    }

    return 0;
}

size_t NeuralNetworkSum::get_n_neurons() {
    //TODO insufficient solution, assumes the networks share weights
    if(this->summand){
        return this->summand->at(0)->get_n_neurons();
    }

    return 0;
}

size_t NeuralNetworkSum::get_n_outputs() {
    //TODO insufficient solution, assumes the networks share weights
    if(this->summand){
        return this->summand->at(0)->get_n_outputs();
    }

    return 0;
}