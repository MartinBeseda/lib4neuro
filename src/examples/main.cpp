/**
 * This file serves for testing of various examples, have fun!
 *
 * @author Michal Kravčenko
 * @date 14.6.18 -
 */

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <utility>
#include <algorithm>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include "Network/NeuralNetwork.h"
#include "Neuron/NeuronLinear.h"
#include "Neuron/NeuronLogistic.h"
#include "NetConnection/ConnectionFunctionIdentity.h"

#include "LearningMethods/ParticleSwarm.h"
#include "Neuron/NeuronBinary.h"
#include "DataSet/DataSet.h"


int main(int argc, char** argv){



    return 0;
}
