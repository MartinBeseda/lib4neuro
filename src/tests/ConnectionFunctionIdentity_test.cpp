/**
* DESCRIPTION OF THE CLASS
*
* @author David Vojtek
* @date 2018
*/

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../NetConnection/ConnectionFunctionIdentity.h"

/**
 * Boost testing suite for testing ConnectionWeightIdentity.h
 */
BOOST_AUTO_TEST_SUITE(ConnectionWeightIdentity_test)

    /**
     * Test of correct construction of ConnectionFunctionIdentity
     */
    BOOST_AUTO_TEST_CASE(ConnectionWeightIdentity_construction_test) {
        std::vector<double> weight_array = {1, 2, 3, 4, 5};
        //Test of none exception when creation new instance of ConnectionFunctionIdentity
        BOOST_CHECK_NO_THROW(ConnectionFunctionIdentity *CFI = new ConnectionFunctionIdentity() );
        BOOST_CHECK_NO_THROW(ConnectionFunctionIdentity *CFI = new ConnectionFunctionIdentity(2) );

    }

    /**
     * Test of eval method
     */
    BOOST_AUTO_TEST_CASE(ConnectionWeightIdentity_eval_test) {
        ConnectionFunctionIdentity *CFI1 = new ConnectionFunctionIdentity();
        ConnectionFunctionIdentity *CFI2 = new ConnectionFunctionIdentity(0);
        ConnectionFunctionIdentity *CFI3 = new ConnectionFunctionIdentity(2);

        std::vector<double> parameter_space;
        parameter_space.push_back(5);

        //Test of correct output of eval method
        BOOST_CHECK_EQUAL(1, CFI1->eval(parameter_space));
        BOOST_CHECK_EQUAL(5, CFI2->eval(parameter_space));
        BOOST_CHECK_THROW(CFI3->eval(parameter_space), std::out_of_range);

    }

BOOST_AUTO_TEST_SUITE_END()
