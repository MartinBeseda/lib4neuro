/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>


#include "../NetConnection/ConnectionFunctionGeneral.h"
#include "../Neuron/NeuronLinear.h"
#include <iostream>
/**
 * Boost testing suite for testing ConnectionFunctionGeneral.h
 */

BOOST_AUTO_TEST_SUITE(Connection_test)

/**
 * Test of constructor of Connection
 */
    BOOST_AUTO_TEST_CASE(Connection_construction__test) {

        BOOST_CHECK_NO_THROW(ConnectionFunctionGeneral *functionGeneral = new ConnectionFunctionGeneral());

        std::vector<size_t> param_indices;
        param_indices.push_back(0);
        std::string paramToFunction = "this do nothing! Why is it here?";
        BOOST_CHECK_NO_THROW(ConnectionFunctionGeneral *functionGeneral = new ConnectionFunctionGeneral(param_indices,
                                                                                                        paramToFunction));
    }


    BOOST_AUTO_TEST_CASE(Connection_eval_test) {
        ConnectionFunctionGeneral *functionGeneral = new ConnectionFunctionGeneral();
        //TODO implementation not finnish yet;
        std::vector<double> parameter_space;
        BOOST_CHECK_EQUAL(0, functionGeneral->eval(parameter_space));
    }

    BOOST_AUTO_TEST_CASE(Connection_eval_partial_derivative_test) {
        //TODO function not implemented yet
    }


BOOST_AUTO_TEST_SUITE_END()