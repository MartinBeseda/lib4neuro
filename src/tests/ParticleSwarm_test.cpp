/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>


#include "../LearningMethods/ParticleSwarm.h"
/**
 * Boost testing suite for testing ParticleSwarm.h
 */

double test_particle_swarm_neural_net_error_function(double *weights){

return 0;
    }

BOOST_AUTO_TEST_SUITE(ParticleSwarm_test)


    BOOST_AUTO_TEST_CASE(ParticleSwarm_construction_test){
        std::vector<double> domain_bound;
        domain_bound.push_back(5);
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);
        ErrorFunction *error = new MSE(&network, &dataSet);
        BOOST_CHECK_NO_THROW(ParticleSwarm swarm(error, &domain_bound, 0, 1, 1, 0, 20));
    }

    BOOST_AUTO_TEST_CASE(ParticleSwarm_optimalize_test){
        std::vector<double> domain_bound;
        domain_bound.push_back(5);
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);
        ErrorFunction *error = new MSE(&network, &dataSet);
        ParticleSwarm swarm(error,&domain_bound, 0, 1, 1, 0, 20);
        BOOST_CHECK_THROW(swarm.optimize(-1,1,1), std::invalid_argument) ;
        BOOST_CHECK_THROW(swarm.optimize(1,-1,1), std::invalid_argument) ;
        BOOST_CHECK_THROW(swarm.optimize(1,1,-1), std::invalid_argument) ;

    }

BOOST_AUTO_TEST_SUITE_END()