/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../ErrorFunction/ErrorFunctions.h"

/**
 * Boost testing suite for testing ErrorFunction.h
 * doesn't test inherited methods
 */
BOOST_AUTO_TEST_SUITE(ErrorFunctions_test)

    BOOST_AUTO_TEST_CASE(ErrorFunction_MSE_Construction_Test) {
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;
        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));
        DataSet dataSet(&data_vec);
        BOOST_CHECK_NO_THROW(MSE mse(&network, &dataSet));
    }

    BOOST_AUTO_TEST_CASE(ErrorFunction_MSE_Eval_Test) {
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;
        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));
        network.add_neuron(n1);
        network.add_neuron(n2);
        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);
        network.randomize_weights();
        std::vector<size_t> net_input_neurons_indices(1);
        std::vector<size_t> net_output_neurons_indices(1);
        net_input_neurons_indices[0] = 0;
        net_output_neurons_indices[0] = 1;
        network.specify_input_neurons(net_input_neurons_indices);
        network.specify_output_neurons(net_output_neurons_indices);

        DataSet dataSet(&data_vec);

        std::vector<double> weights;
        weights.push_back(1);

        MSE mse(&network, &dataSet);

        BOOST_CHECK_EQUAL(9, mse.eval(&weights));
    }

    BOOST_AUTO_TEST_CASE(ErrorFunction_MSE_Get_dimension_test) {
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;
        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));
        network.add_neuron(n1);
        network.add_neuron(n2);
        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);
        network.randomize_weights();
        std::vector<size_t> net_input_neurons_indices(1);
        std::vector<size_t> net_output_neurons_indices(1);
        net_input_neurons_indices[0] = 0;
        net_output_neurons_indices[0] = 1;
        network.specify_input_neurons(net_input_neurons_indices);
        network.specify_output_neurons(net_output_neurons_indices);

        DataSet dataSet(&data_vec);

        MSE mse(&network, &dataSet);

        BOOST_CHECK_EQUAL(2, mse.get_dimension());
    }

    BOOST_AUTO_TEST_CASE(ErrorFunction_MSE_SUM_Construction_Test) {
        BOOST_CHECK_NO_THROW(ErrorSum mse_sum);
    }

    BOOST_AUTO_TEST_CASE(ErrorFunction_MSE_SUM_Add_Error_Function_Test) {
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;
        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));
        DataSet dataSet(&data_vec);

        ErrorFunction *f = new MSE(&network, &dataSet);

        ErrorSum mse_sum;
        BOOST_CHECK_NO_THROW(mse_sum.add_error_function(f));
    }

    BOOST_AUTO_TEST_CASE(ErrorFunction_MSE_SUM_Eval_Test) {
        ErrorSum mse_sum;

        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;
        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));
        network.add_neuron(n1);
        network.add_neuron(n2);
        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);
        network.randomize_weights();
        std::vector<size_t> net_input_neurons_indices(1);
        std::vector<size_t> net_output_neurons_indices(1);
        net_input_neurons_indices[0] = 0;
        net_output_neurons_indices[0] = 1;
        network.specify_input_neurons(net_input_neurons_indices);
        network.specify_output_neurons(net_output_neurons_indices);

        DataSet dataSet(&data_vec);

        ErrorFunction *f = new MSE(&network, &dataSet);

        mse_sum.add_error_function(f);

        std::vector<double> weights;
        weights.push_back(1);

        BOOST_CHECK_EQUAL(9, mse_sum.eval(&weights));
    }

    BOOST_AUTO_TEST_CASE(ErrorFunction_MSE_SUM_Get_Dimension_test) {
        ErrorSum mse_sum;
        BOOST_CHECK_EQUAL(0, mse_sum.get_dimension());

        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;
        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));
        network.add_neuron(n1);
        network.add_neuron(n2);
        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);
        network.randomize_weights();
        std::vector<size_t> net_input_neurons_indices(1);
        std::vector<size_t> net_output_neurons_indices(1);
        net_input_neurons_indices[0] = 0;
        net_output_neurons_indices[0] = 1;
        network.specify_input_neurons(net_input_neurons_indices);
        network.specify_output_neurons(net_output_neurons_indices);

        DataSet dataSet(&data_vec);

        ErrorFunction *f = new MSE(&network, &dataSet);

        mse_sum.add_error_function(f);

        BOOST_CHECK_EQUAL(2, mse_sum.get_dimension());

    }


BOOST_AUTO_TEST_SUITE_END()