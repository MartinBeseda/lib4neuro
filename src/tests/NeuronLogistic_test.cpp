/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../Neuron/NeuronLogistic.h"

/**
 * Boost testing suite for testing NeuronLogistic.h
 * doesn't test inherited methods
 */
BOOST_AUTO_TEST_SUITE(neuronLogistic_test)

    /**
     * Test of creating new instance of NeuronLogistic
     */
    BOOST_AUTO_TEST_CASE(neuronLogistic_construction__test) {
        BOOST_CHECK_NO_THROW(NeuronLogistic *neuron = new NeuronLogistic());

    }

    /**
     * Test of activate method
     */
    BOOST_AUTO_TEST_CASE(neuronLogistic_activate__test) {
        NeuronLogistic *neuron = new NeuronLogistic();

        //Test of correct state after activate neuron
        BOOST_CHECK_CLOSE(0.73105857863, neuron->activate(3.0,2.0), 0.00001);
    }

    /**
     * Test of derivative methods
     */
    BOOST_AUTO_TEST_CASE(neuronLogistic_derivative_test) {
        NeuronLogistic *neuron = new NeuronLogistic();
        //3.0 2.0
        //Test of correct output of activation_function_get_derivative method
        BOOST_CHECK_CLOSE(0.196611933241, neuron->activation_function_eval_derivative(3.0,2.0), 0.00001);
        BOOST_CHECK_CLOSE(-0.196611933241, neuron->activation_function_eval_derivative_bias(3.0,2.0), 0.00001);

    }

    BOOST_AUTO_TEST_CASE(neuronLogistic_d1_construction__test) {
        BOOST_CHECK_NO_THROW(NeuronLogistic_d1 *neuron = new NeuronLogistic_d1());
    }

    /**
     * Test of activate method
     */
    BOOST_AUTO_TEST_CASE(neuronLogistic_d1_activate__test) {
        NeuronLogistic_d1 *neuron = new NeuronLogistic_d1();

        //Test of correct state after activate neuron
        BOOST_CHECK_CLOSE(0.196611933241, neuron->activate(3.0,2.0), 0.00001);
    }

    /**
     * Test of derivative methods
     */
    BOOST_AUTO_TEST_CASE(neuronLogistic_d1_derivative_test) {
        NeuronLogistic_d1 *neuron = new NeuronLogistic_d1();
        //3.0 2.0
        //Test of correct output of activation_function_get_derivative method
        BOOST_CHECK_CLOSE(-0.0908577476729, neuron->activation_function_eval_derivative(3.0,2.0), 0.00001);
        BOOST_CHECK_CLOSE(0.0908577476729, neuron->activation_function_eval_derivative_bias(3.0,2.0), 0.00001);
    }

    BOOST_AUTO_TEST_CASE(neuronLogistic_d2_construction__test) {
        BOOST_CHECK_NO_THROW(NeuronLogistic_d2 *neuron = new NeuronLogistic_d2());
    }

    /**
     * Test of activate method
     */
    BOOST_AUTO_TEST_CASE(neuronLogistic_d2_activate__test) {
        NeuronLogistic_d2 *neuron = new NeuronLogistic_d2();

        //Test of correct state after activate neuron
        BOOST_CHECK_CLOSE(-0.0908577476729, neuron->activate(3.0,2.0), 0.00001);
    }

    /**
     * Test of derivative methods
     */
    BOOST_AUTO_TEST_CASE(neuronLogistic_d2_derivative_test) {
        NeuronLogistic_d2 *neuron = new NeuronLogistic_d2();
        //3.0 2.0
        //Test of correct output of activation_function_get_derivative method
        BOOST_CHECK_CLOSE(-0.03532558051623, neuron->activation_function_eval_derivative(3.0,2.0), 0.00001);
        BOOST_CHECK_CLOSE(0.03532558051623, neuron->activation_function_eval_derivative_bias(3.0,2.0), 0.00001);
    }

BOOST_AUTO_TEST_SUITE_END()
