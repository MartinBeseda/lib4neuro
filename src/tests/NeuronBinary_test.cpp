/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../Neuron/NeuronBinary.h"

/**
 * Boost testing suite for testing NeuronBinary.h
 * doesn't test inherited methods
 */
BOOST_AUTO_TEST_SUITE(neuronBinary_test)

    /**
     * Test of creating new instance of NeuronBinary
     */
    BOOST_AUTO_TEST_CASE(neuronBinary_construction_test) {

        BOOST_CHECK_NO_THROW(NeuronBinary *neuron = new NeuronBinary());
    }

    /**
     * Test of activate method
     */
    BOOST_AUTO_TEST_CASE(neuronBinary_activate_test) {
        NeuronBinary *neuron = new NeuronBinary();

        //Test of correct state neuron
        BOOST_CHECK_EQUAL(0.0, neuron->activate(2.0, 3.0));
        BOOST_CHECK_EQUAL(1.0, neuron->activate(3.0, 3.0));
        BOOST_CHECK_EQUAL(1.0, neuron->activate(3.0, 2.0));


    }

BOOST_AUTO_TEST_SUITE_END()