/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include <iostream>
#include <boost/test/output_test_stream.hpp>
#include "../Solvers/DESolver.h"

/**
 * Boost testing suite for testing DESolver.h
 *
 */
BOOST_AUTO_TEST_SUITE(DESolver_test)

    /**
     * Test of MultiIndex construction test
     */
    BOOST_AUTO_TEST_CASE(MultiIndex_construction_test) {
        BOOST_CHECK_NO_THROW(MultiIndex multiIndex(2));
    }

    /**
     * Test of MultiIndex set_partial_deravitive method
     */
    BOOST_AUTO_TEST_CASE(MultiIndex_set_partial_derivative_test) {
        MultiIndex multiIndex(2);
        BOOST_CHECK_NO_THROW(multiIndex.set_partial_derivative(0, 1));
        BOOST_CHECK_NO_THROW(multiIndex.set_partial_derivative(1, 2));
        //BOOST_CHECK_THROW(multiIndex.set_partial_derivative(2, 3), std::out_of_range);
    }

    /**
     * Testo of MultiIndex get_partial_derivative_degrees method
     */
    BOOST_AUTO_TEST_CASE(MultiIndex_get_partial_derivative_degrees_test) {
        MultiIndex multiIndex(2);
        multiIndex.set_partial_derivative(0, 1);
        multiIndex.set_partial_derivative(1, 2);

        BOOST_CHECK_EQUAL(1, multiIndex.get_partial_derivatives_degrees()->at(0));
        BOOST_CHECK_EQUAL(2, multiIndex.get_partial_derivatives_degrees()->at(1));
    }

    /**
     * Test of MultiIndex operator< method
     */
    BOOST_AUTO_TEST_CASE(MultiIndex_operator_test) {
        MultiIndex multiIndex1(1);
        multiIndex1.set_partial_derivative(0, 1);
        MultiIndex multiIndex2(2);
        multiIndex2.set_partial_derivative(0, 1);
        multiIndex2.set_partial_derivative(1, 2);
        MultiIndex multiIndex3(1);
        multiIndex3.set_partial_derivative(0, 2);

        BOOST_CHECK(multiIndex1.operator<(multiIndex2));
        //BOOST_CHECK_THROW(multiIndex2.operator<(multiIndex1), std::out_of_range);
        BOOST_CHECK(!multiIndex1.operator<(multiIndex1));
        BOOST_CHECK(multiIndex1.operator<((multiIndex3)));
    }

    /**
     * Test of MultiIndex toString method
     */
    BOOST_AUTO_TEST_CASE(MultiIndex_toString_test) {
        MultiIndex multiIndex(2);
        BOOST_CHECK_EQUAL("0, 0", multiIndex.to_string());
    }

    /**
     * Test of MultiIndex get_degree method
     */
    BOOST_AUTO_TEST_CASE(MultiIndex_get_degree_test) {
        MultiIndex multiIndex(2);
        BOOST_CHECK_EQUAL(0, multiIndex.get_degree());

        multiIndex.set_partial_derivative(0, 1);
        multiIndex.set_partial_derivative(1, 3);

        BOOST_CHECK_EQUAL(4, multiIndex.get_degree());
    }

    /**
     * Test of DESolver construction
     */
    BOOST_AUTO_TEST_CASE(DESolver_construction_test) {
        BOOST_CHECK_THROW(DESolver(0, 1, 1), std::invalid_argument);
        BOOST_CHECK_THROW(DESolver(1, 0, 1), std::invalid_argument);
        BOOST_CHECK_THROW(DESolver(1, 1, 0), std::invalid_argument);
        BOOST_CHECK_NO_THROW(DESolver deSolver(1, 1, 1));

        /*boost::test_tools::output_test_stream output;
        {
            cout_redirect guard(output.rdbuf());
            DESolver deSolver(1,1,1,1);
        }
        BOOST_CHECK(output.is_equal("Differential Equation Solver with 1 equations\n--------------------------------------------------------------------------\nConstructing NN structure representing the solution [1 input neurons][1 inner neurons][1 output neurons]...\n  adding a connection between input neuron  0 and inner neuron  0, weight index 0\n  adding a connection between inner neuron  0 and output neuron  0, weight index 1\ndone\n\n"));
        */
    }

    /**
     * Test of DESolver get_solution method
     */
    BOOST_AUTO_TEST_CASE(DESolver_get_solution_test) {
        DESolver deSolver(1, 1, 1);
        MultiIndex *alpha = new MultiIndex(1);
        NeuralNetwork *network = deSolver.get_solution(*alpha);
        BOOST_CHECK_EQUAL(1, network->get_n_inputs());
        BOOST_CHECK_EQUAL(1, network->get_n_outputs());
    }

    BOOST_AUTO_TEST_CASE(DESolver_add_eq_test){
        DESolver *deSolver = new DESolver(1,1,1);
        MultiIndex *multiIndex = new MultiIndex(2);
        multiIndex->set_partial_derivative(0,1);
        multiIndex->set_partial_derivative(1,0.5);

        deSolver->add_to_differential_equation(0, *multiIndex, 0.5);

        std::vector<double> inp, out;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec_dy;
        inp = {0.0};
        out = {8.0};
        data_vec_dy.emplace_back(std::make_pair(inp, out));
        DataSet ds_02(&data_vec_dy);

        deSolver->set_error_function( 0, ErrorFunctionType::ErrorFuncMSE, &ds_02 );

        std::vector<double> weights;
        weights.push_back(1.0);
        BOOST_CHECK_EQUAL(64,deSolver->eval_total_error(weights));
    }

BOOST_AUTO_TEST_SUITE_END()
