/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../Network/NeuralNetworkSum.h"

/**
 * Boost testing suite for testing NeuralNetworkSum.h
 */
BOOST_AUTO_TEST_SUITE(NeuralNetworkSum_test)

    /**
     * Test of creating new instance of NeuralNetworkSum
     */
    BOOST_AUTO_TEST_CASE(NeuralNetworkSum_constuction_test) {
        //Test of none exception raise when creating new instance of NeuralNewtwork
        BOOST_CHECK_NO_THROW(NeuralNetworkSum networkSum);
    }

    BOOST_AUTO_TEST_CASE(NeuralNetworkSum_add_network_test) {
        NeuralNetwork network;
        NeuralNetworkSum networkSum;

        BOOST_CHECK_NO_THROW(networkSum.add_network(&network, 5));
    }

    BOOST_AUTO_TEST_CASE(NeuralNetworkSum_eval_single_weights_test) {
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        network.add_neuron(n1);
        network.add_neuron(n2);

        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);

        std::vector<size_t> output_neuron_indices(1);
        output_neuron_indices[0] = (size_t) 1;
        network.specify_output_neurons(output_neuron_indices);

        std::vector<size_t> input_neuron_indices(1);
        input_neuron_indices[0] = (size_t) 0;
        network.specify_input_neurons(input_neuron_indices);

        std::vector<double> input;
        input.push_back(1);
        std::vector<double> output;
        output.push_back(1);

        double weights = 5;
        NeuralNetworkSum networkSum;
        networkSum.add_network(&network, 2);

        networkSum.eval_single(input, output);
        BOOST_CHECK_EQUAL(2, output.at(0));
    }

    BOOST_AUTO_TEST_CASE(NeuralNetworkSum_get_weights_test) {
        NeuralNetworkSum networkSum;
        BOOST_CHECK_EQUAL(0, networkSum.get_n_weights());

        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        network.add_neuron(n1);
        network.add_neuron(n2);

        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::NEXT_WEIGHT, 2.5);

        networkSum.add_network(&network, 2);

        BOOST_CHECK_EQUAL(1, networkSum.get_n_weights());
    }


BOOST_AUTO_TEST_SUITE_END()