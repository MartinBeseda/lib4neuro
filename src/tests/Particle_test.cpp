/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../LearningMethods/ParticleSwarm.h"
/**
 * Boost testing suite for testing ParticleSwarm.h
 * TODO
 */
 BOOST_AUTO_TEST_SUITE(Particle_test)

    BOOST_AUTO_TEST_CASE(Particle_construction_test) {
        double domain_bound[5] = {1, 2, 3, 4, 5};
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));
        network.add_neuron(n1);
        network.add_neuron(n2);
        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);
        network.randomize_weights();

        std::vector<size_t> net_input_neurons_indices(1);
        std::vector<size_t> net_output_neurons_indices(1);
        net_input_neurons_indices[0] = 0;

        net_output_neurons_indices[0] = 1;

        network.specify_input_neurons(net_input_neurons_indices);
        network.specify_output_neurons(net_output_neurons_indices);

        DataSet dataSet(&data_vec);
        ErrorFunction *error = new MSE(&network, &dataSet);
        Particle particle(error, &domain_bound[0]);
        BOOST_CHECK_NO_THROW(Particle particle(error, &domain_bound[0]));
        //  particle.get_coordinate();
    }

    BOOST_AUTO_TEST_CASE(Particle_get_coordinate_test) {
        double domain_bound[5] = {1, 2, 3, 4, 5};
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));
        network.add_neuron(n1);
        network.add_neuron(n2);
        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);
        network.randomize_weights();

        std::vector<size_t> net_input_neurons_indices(1);
        std::vector<size_t> net_output_neurons_indices(1);
        net_input_neurons_indices[0] = 0;

        net_output_neurons_indices[0] = 1;

        network.specify_input_neurons(net_input_neurons_indices);
        network.specify_output_neurons(net_output_neurons_indices);

        DataSet dataSet(&data_vec);
        ErrorFunction *error = new MSE(&network, &dataSet);
        Particle particle1(error, &domain_bound[0]);
        Particle particle2(error, &domain_bound[0]);

        BOOST_CHECK(*particle1.get_coordinate() != *particle2.get_coordinate());
    }

    //Random
    //TODO
     /*
     BOOST_AUTO_TEST_CASE(particle_change_coordiante_test) {
         double domain_bound[5] = {1,2,3,4,5};
         Neuron *n1 = new NeuronLinear(1, 1);
         Neuron *n2 = new NeuronLinear(2, 2);
         NeuralNetwork network;
         std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
         std::vector<double> inp, out;

         for (int i = 0; i < 1; i++) {
             inp.push_back(i);
             out.push_back(i + 4);
         }

         data_vec.emplace_back(std::make_pair(inp, out));
         network.add_neuron(n1);
         network.add_neuron(n2);
         network.add_connection_simple(0, 1, 0, 2.5);
         network.randomize_weights();

         std::vector<size_t> net_input_neurons_indices(1);
         std::vector<size_t> net_output_neurons_indices(1);
         net_input_neurons_indices[0] = 0;

         net_output_neurons_indices[0] = 1;

         network.specify_input_neurons(net_input_neurons_indices);
         network.specify_output_neurons(net_output_neurons_indices);



         DataSet dataSet(&data_vec);
         ErrorFunction *error = new MSE(&network, &dataSet);
         Particle particle(error, &domain_bound[0]);
         particle.change_coordinate(1.0, 2.0, 2.0, &domain_bound[1], 1);


         BOOST_CHECK_EQUAL(1.32664, *particle.get_coordinate());
     }

     BOOST_AUTO_TEST_CASE(particle_optimal_value_test){
         double domain_bound[5] = {1,2,3,4,5};
         Neuron *n1 = new NeuronLinear(1, 1);
         Neuron *n2 = new NeuronLinear(2, 2);
         NeuralNetwork network;
         std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
         std::vector<double> inp, out;

         for (int i = 0; i < 1; i++) {
             inp.push_back(i);
             out.push_back(i + 4);
         }

         data_vec.emplace_back(std::make_pair(inp, out));
         network.add_neuron(n1);
         network.add_neuron(n2);
         network.add_connection_simple(0, 1, 0, 2.5);
         network.randomize_weights();

         std::vector<size_t> net_input_neurons_indices(1);
         std::vector<size_t> net_output_neurons_indices(1);
         net_input_neurons_indices[0] = 0;

         net_output_neurons_indices[0] = 1;

         network.specify_input_neurons(net_input_neurons_indices);
         network.specify_output_neurons(net_output_neurons_indices);

         DataSet dataSet(&data_vec);
         ErrorFunction *error = new MSE(&network, &dataSet);
         Particle particle(error, &domain_bound[0]);
         BOOST_CHECK_CLOSE(1.789708839, particle.get_optimal_value(), 0.00001 );
     }
     */


BOOST_AUTO_TEST_SUITE_END()