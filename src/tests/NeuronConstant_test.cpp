/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../Neuron/NeuronConstant.h"

/**
 * Boost testing suite for testing NeuronConstant.h
 * doesn't test inherited methods
 */
BOOST_AUTO_TEST_SUITE(neuronConstant_test)

    /**
     * Test of creating new instance of NeuronConstant
     */
    BOOST_AUTO_TEST_CASE(neuronConstant_construction_test) {
        BOOST_CHECK_NO_THROW(NeuronConstant *neuron = new NeuronConstant(2.0));
        BOOST_CHECK_NO_THROW(NeuronConstant *neuron = new NeuronConstant());

    }

    /**
     * Test of activate method
     */
    BOOST_AUTO_TEST_CASE(neuronConstant_activate__test) {
        NeuronConstant *neuron = new NeuronConstant(2.0);
        //Test of correct state after activate neuron
        BOOST_CHECK_EQUAL(2.0, neuron->activate(8.0, 7.0));
        
        NeuronConstant *neuron2 = new NeuronConstant();
        //Test of correct state after activate neuron
        BOOST_CHECK_EQUAL(0.0, neuron2->activate(8.0, 7.0));
    }

    /**
     * Test of derivative methods
     */
    BOOST_AUTO_TEST_CASE(neuronConstant_derivative_test) {
        NeuronConstant *neuron = new NeuronConstant(2.0);

        //Test of correct output of activation_function_get_derivative method
        BOOST_CHECK_EQUAL(0.0, neuron->activation_function_eval_derivative(3.0, 2.0));
        BOOST_CHECK_EQUAL(0.0, neuron->activation_function_eval_derivative_bias(3.0, 2.0));
    }

BOOST_AUTO_TEST_SUITE_END()
