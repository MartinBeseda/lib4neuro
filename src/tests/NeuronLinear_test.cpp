/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../Neuron/NeuronLinear.h"

/**
 * Boost testing suite for testing NeuronLinear.h
 * doesn't test inherited methods
 */
BOOST_AUTO_TEST_SUITE(neuronLinear_test)

    /**
     * Test of creating new instance of NeuronLinear
     */
    BOOST_AUTO_TEST_CASE(neuronLinear_construction_test) {
        BOOST_CHECK_NO_THROW(NeuronLinear *neuron = new NeuronLinear());
    }

    /**
     * Test of activate method
     */
    BOOST_AUTO_TEST_CASE(neuronLinear_activate_test) {
        NeuronLinear *neuron = new NeuronLinear();
        //Test of correct state after activate neuron
        BOOST_CHECK_EQUAL(5.0, neuron->activate(3.0, 2.0));
    }

    /**
     * Test of derivative methods
     */
    BOOST_AUTO_TEST_CASE(neuronLinear_derivative_test) {
        NeuronLinear *neuron = new NeuronLinear();

        //Test of correct output of activation_function_get_derivative method
        BOOST_CHECK_EQUAL(1.0, neuron->activation_function_eval_derivative(3.0, 2.0));
        BOOST_CHECK_EQUAL(1.0, neuron->activation_function_eval_derivative_bias(3.0, 2.0));
    }

BOOST_AUTO_TEST_SUITE_END()
