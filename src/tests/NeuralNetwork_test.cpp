/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include "../Network/NeuralNetwork.h"
#include <boost/test/output_test_stream.hpp>

struct cerr_redirect {
    cerr_redirect(std::streambuf *new_buffer)
            : old(std::cerr.rdbuf(new_buffer)
    ) {}

    ~cerr_redirect() {
        std::cout.rdbuf(old);
    }

private:
    std::streambuf *old;
};

/**
 * Boost testing suite for testing NeuralNetwork.h
 */
BOOST_AUTO_TEST_SUITE(NeuralNetwork_test)

    /**
     * Test of creating new instance of NeuralNetwork
     */
    BOOST_AUTO_TEST_CASE(NeuralNetwork_constuction_test) {
        //Test of none exception raise when creating new instance of NeuralNewtwork
        BOOST_CHECK_NO_THROW(NeuralNetwork network);
    }

    /**
     * Test of add_neuron method
     * Existing bias out of range cancelation
     */
    BOOST_AUTO_TEST_CASE(NeuralNetwork_add_neuron_test) {
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        Neuron *n3 = new NeuronLinear();
        Neuron *n4 = new NeuronLinear();

        NeuralNetwork network;

        //Tests of correct neuron indexs when add_neuron
        BOOST_CHECK_EQUAL(0, network.add_neuron(n1));

        BOOST_CHECK_EQUAL(1, network.add_neuron(n2, BIAS_TYPE::NEXT_BIAS));

        BOOST_CHECK_EQUAL(2, network.add_neuron(n4, BIAS_TYPE::NO_BIAS));

        BOOST_CHECK_EQUAL(3, network.get_n_neurons());

        BOOST_CHECK_EQUAL(2, network.get_n_biases());

//////TODO fix dumping stack error
//        boost::test_tools::output_test_stream output;
//        {
//            cerr_redirect guard(output.rdbuf());
//          network.add_neuron(n3, BIAS_TYPE::EXISTING_BIAS, 3);
//        }
//        BOOST_CHECK(output.is_equal("The supplied bias index is too large!\n\n"));
    }

    /**
     * Test of add_connection_simple method
     */
    BOOST_AUTO_TEST_CASE(NeuralNetwork_add_connection_simple_test) {
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        network.add_neuron(n1, BIAS_TYPE::NO_BIAS);
        network.add_neuron(n2, BIAS_TYPE::NO_BIAS);

        BOOST_CHECK_EQUAL(0, network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::NEXT_WEIGHT));
        BOOST_CHECK_EQUAL(1, network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::NEXT_WEIGHT, 5));
        BOOST_CHECK_EQUAL(2, network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT));
        BOOST_CHECK_EQUAL(3, network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 5));
        BOOST_CHECK_EQUAL(4, network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::EXISTING_WEIGHT, 1));

        BOOST_CHECK_EQUAL(2, network.get_n_weights());
//TODO fix dumping stack error
//        boost::test_tools::output_test_stream output;
//        {
//            cerr_redirect guard(output.rdbuf());
//            network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::EXISTING_WEIGHT,10);
//        }
//        BOOST_CHECK(output.is_equal("The supplied connection weight index is too large!\n\n"));
    }

    /**
     * Test of add_connection_general method
     */
    BOOST_AUTO_TEST_CASE(NeuralNetwork_specify_inputs_neurons_test) {
        NeuralNetwork network;
        Neuron *n1 = new NeuronLinear();
        network.add_neuron(n1, BIAS_TYPE::NO_BIAS);

        std::vector<size_t> inputs;
        inputs.push_back(0);

        BOOST_CHECK_EQUAL(0, network.get_n_inputs());
        network.specify_input_neurons(inputs);
        BOOST_CHECK_EQUAL(1, network.get_n_inputs());
    }

    BOOST_AUTO_TEST_CASE(NeuralNetwork_specify_outputs_neurons_test) {
        NeuralNetwork network;
        Neuron *n1 = new NeuronLinear();
        network.add_neuron(n1, BIAS_TYPE::NO_BIAS);

        std::vector<size_t> outputs;
        outputs.push_back(0);

        BOOST_CHECK_EQUAL(0, network.get_n_outputs());
        network.specify_output_neurons(outputs);
        BOOST_CHECK_EQUAL(1, network.get_n_outputs());
    }

    BOOST_AUTO_TEST_CASE(NeuralNetwork_eval_single_test) {
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();

        NeuralNetwork network;
        network.add_neuron(n1);
        network.add_neuron(n2);

        network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::UNITARY_WEIGHT, 2.5);

        std::vector<size_t> output_neuron_indices;
        output_neuron_indices.push_back(1);
        network.specify_output_neurons(output_neuron_indices);

        std::vector<size_t> input_neuron_indices;
        input_neuron_indices.push_back(0);
        network.specify_input_neurons(input_neuron_indices);

        std::vector<double> input;
        input.push_back(5);
        std::vector<double> output;
        output.push_back(8);

        network.eval_single(input, output);
        BOOST_CHECK_EQUAL(5, output.at(0));
    }

    BOOST_AUTO_TEST_CASE(NeuralNetwork_randomize_weights_test) {
        Neuron *n1 = new NeuronLinear();
        Neuron *n2 = new NeuronLinear();
        NeuralNetwork network;
        network.add_neuron(n1, BIAS_TYPE::NO_BIAS);
        network.add_neuron(n2, BIAS_TYPE::NO_BIAS);

        for (int i = 0; i < 100; i++) {
            network.add_connection_simple(0, 1, SIMPLE_CONNECTION_TYPE::NEXT_WEIGHT);
        }
        network.randomize_weights();
        std::vector<double> *weights = network.get_parameter_ptr_weights();

        double sum=0;

        for (int i = 0; i < 100; i++) {
            sum += weights->at(i);
        }
        sum=sum/100;
        BOOST_CHECK(sum<0.15 && sum>-0.15);
    }

BOOST_AUTO_TEST_SUITE_END()