/**
 * DESCRIPTION OF THE CLASS
 *
 * @author David Vojtek
 * @date 2018
 */

#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>
#include "../DataSet/DataSet.h"
#include "stdio.h"
#include <iostream>
//#include <boost/filesystem.hpp>


/**
 * Boost testing suite for testing DataSet.h
 */
BOOST_AUTO_TEST_SUITE(DataSet_test)

    struct cout_redirect {
        cout_redirect(std::streambuf *new_buffer)
                : old(std::cout.rdbuf(new_buffer)) {}

        ~cout_redirect() {
            std::cout.rdbuf(old);
        }

    private:
        std::streambuf *old;
    };

/**
 * Test of DataSet constructor with filepath parameter
 */
    BOOST_AUTO_TEST_CASE(DataSet_construction_from_file_test) {
        //test of exception with non-existing file path
        //TODO resolve exception throw
      //  DataSet dataSet("file/unknown");
        //BOOST_CHECK_THROW(DataSet dataSet("file unknown"), boost::archive::archive_exception::input_stream_error);
    }

/**
 * Test of DataSet constructor with vector parameter
 */
    BOOST_AUTO_TEST_CASE(DataSet_construction_from_vector_test) {
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);

        //test of no exception when create object DataSet
        BOOST_CHECK_NO_THROW(DataSet dataSet(&data_vec));
    }

/**
 * Test of get_data method
 */
    BOOST_AUTO_TEST_CASE(DataSet_get_data_test) {
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));
        DataSet dataSet(&data_vec);

        //test of equal data
        //TODO out of range, ==
       BOOST_CHECK_EQUAL(0, dataSet.get_data()->at(0).first.at(0));
       BOOST_CHECK_EQUAL(4, dataSet.get_data()->at(0).second.at(0));

    }

/**
 * Test of add_data_pair method
 */
    BOOST_AUTO_TEST_CASE(DataSet_add_daata_pair_test) {
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);

        inp.clear();
        out.clear();
        for (int i = 8; i < 11; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        dataSet.add_data_pair(inp, out);

        // Test of correct add of input
        BOOST_CHECK_EQUAL(8, dataSet.get_data()->at(1).first.at(0));
        // Test of correct add of output
        BOOST_CHECK_EQUAL(12, dataSet.get_data()->at(1).second.at(0));

    }

    /**
     * Test of get_input_dim and get_output_dim methods
     */
    BOOST_AUTO_TEST_CASE(DataSet_dimension_test) {
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);

        //Test of correct input dimension
        BOOST_CHECK_EQUAL(3, dataSet.get_input_dim());
        //Test of correct output dimension
        BOOST_CHECK_EQUAL(3, dataSet.get_output_dim());
    }

/**
 * Test of get_n_elements method
 */
    BOOST_AUTO_TEST_CASE(DataSet_get_number_of_elements_test) {
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));
        inp.clear();
        out.clear();
        for (int i = 8; i < 11; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }
        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);

        //Test of correct number of elements
        BOOST_CHECK_EQUAL(2, dataSet.get_n_elements());
    }

/**
 * Test of print_data method
 */
    BOOST_AUTO_TEST_CASE(DataSet_print_data_test) {
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 1; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);

        boost::test_tools::output_test_stream output;
        {
            cout_redirect guard(output.rdbuf());
            dataSet.print_data();
        }

        //Test of correct print of DataSet
        BOOST_CHECK(output.is_equal("0 -> 4 \n"));
    }

/**
 * Test of store_text method
 */
    BOOST_AUTO_TEST_CASE(DataSet_store_text_test) {
        std::vector<std::pair<std::vector<double>, std::vector<double>>> data_vec;
        std::vector<double> inp, out;

        for (int i = 0; i < 3; i++) {
            inp.push_back(i);
            out.push_back(i + 4);
        }

        data_vec.emplace_back(std::make_pair(inp, out));

        DataSet dataSet(&data_vec);
        int elements = dataSet.get_n_elements();
        std::string filename = "testDataSet";
        dataSet.store_text(filename);

        //Test of correct file creations
        //BOOST_CHECK(boost::filesystem::exists( "testDataSet" ));

        DataSet newDataSet("testDataSet");

        //Test of correct number of element from dataSet from file
        BOOST_CHECK_EQUAL(elements, newDataSet.get_n_elements());

        // removing of created file
        remove("testDataSet");
    }

BOOST_AUTO_TEST_SUITE_END()