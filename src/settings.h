/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 24.7.18 -
 */

#ifndef INC_4NEURO_SETTINGS_H
#define INC_4NEURO_SETTINGS_H

/**
 * If defined, the NN feed-forward will print out whats happening
 */
//#define VERBOSE_NN_EVAL


#endif //INC_4NEURO_SETTINGS_H
