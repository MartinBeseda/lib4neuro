/**
 * File containing methods for quick & simple formulation of PDEs as a system of Neural Networks with shared weights
 *
 * @author Michal Kravčenko
 * @date 22.7.18 -
 */

 //TODO incorporate uncertainities as coefficients in NeuralNetworkSum or ErrorSum
 //TODO add support for multiple unknown functions to be found
 //TODO add export capability?
 //TODO restructure of the learning methods to have a common parent to be used as a parameter in the solvers

#ifndef INC_4NEURO_PDESOLVER_H
#define INC_4NEURO_PDESOLVER_H

#include <map>
#include "../DataSet/DataSet.h"
#include "../Network/NeuralNetwork.h"
#include "../Network/NeuralNetworkSum.h"
#include "../ErrorFunction/ErrorFunctions.h"
#include "../Neuron/NeuronLinear.h"
#include "../Neuron/NeuronLogistic.h"
#include "../LearningMethods/ParticleSwarm.h"

/**
 * class representing a multi-index of partial derivatives
 */
class MultiIndex{
private:
    /**
     * total number of variables
     */
    size_t dim;

    /**
     * a vector containing degrees of partial derivatives with respect to each variable
     */
    std::vector<size_t> partial_derivatives_degrees;

public:
    /**
     *
     * @param dimension
     */
    MultiIndex(size_t dimension);


    /**
     *
     * @param index
     * @param value
     */
    void set_partial_derivative(size_t index, size_t value);

    /**
     *
     * @return
     */
    std::vector<size_t>* get_partial_derivatives_degrees( ) ;


    /**
     *
     * @param rhs
     * @return
     */
    bool operator <(const MultiIndex& rhs) const;

    /**
     *
     * @return
     */
    std::string to_string( ) const ;

    /**
     *
     * @return
     */
    size_t get_degree( ) const ;
};



class DESolver {
private:

    /* Mapping between used multiindices of partial derivatices and already defined NNs (in order to not define
     * the same NN multiple times )*/
    std::map<MultiIndex, NeuralNetwork*> map_multiindices2nn;

    /* A list of the differential equations */
    std::vector<NeuralNetworkSum*> * differential_equations = nullptr;

    /* Error functions for differential equations */
    std::vector<ErrorFunctionType> * errors_functions_types = nullptr;
    std::vector<DataSet*> * errors_functions_data_sets = nullptr;

    /* NN as the unknown function */
    NeuralNetwork * solution = nullptr;

    /* auxilliary variables */
    std::vector<NeuronLogistic*> *solution_inner_neurons = nullptr;
    size_t dim_i = 0, dim_inn = 0, n_equations = 0;

public:
    /**
     * The attempted solution will contain 'm' inner neurons between the input neurons and the output neuron
     * @param n_equations
     * @param n_inputs
     * @param m
     */
    DESolver( size_t n_equations, size_t n_inputs, size_t m );

    /**
     * default destructor
     */
    ~DESolver( );

    /**
     * Adds a new summand multiplied by 'beta' into the 'equation_idx'-th differential equation
     * @param equation_idx
     * @param alpha
     * @param beta
     */
    void add_to_differential_equation( size_t equation_idx, MultiIndex &alpha, double beta );

    /**
     * Sets the error function for the differential equation with the corresponding index
     * @param equation_idx
     * @param F
     * @param conditions
     */
    void set_error_function(size_t equation_idx, ErrorFunctionType F, DataSet *conditions);



    void solve_via_particle_swarm(
            std::vector<double> *domain_bounds,
            double c1,
            double c2,
            double w,
            size_t n_particles,
            size_t max_iters,
            double gamma,
            double epsilon,
            double delta
            );

    /**
     * returns the pointer to the object representing the given partial derivative of the solution
     * @return
     */
    NeuralNetwork* get_solution( MultiIndex &alpha );

    /**
     * For testing purposes only
     */
     double eval_equation( size_t equation_idx, std::vector<double> *weights_and_biases, std::vector<double> &input );

     /**
      * For testing purposes only
      * @return
      */
     double eval_total_error( std::vector<double> &weights_and_biases );
};


#endif //INC_4NEURO_PDESOLVER_H
