/**
 * This file contains an interface for all learning methods in the library
 *
 * @author Michal Kravčenko
 * @date 12.8.18 -
 */

#ifndef LIB4NEURO_ILEARNINGMETHODS_H
#define LIB4NEURO_ILEARNINGMETHODS_H

#include <vector>
#include "../ErrorFunction/ErrorFunctions.h"

class ILearningMethods {
public:
    /*
     * Runs the method specific learning algorithm minimizing the given error function
     */
    virtual void optimize( ErrorFunction &error ) = 0;

    /*
     * Updates the optimal weight&bias settings in the passed vector
     */
    virtual void get_optimal_configuration( std::vector<double> &config ) = 0;
};


#endif //LIB4NEURO_ILEARNINGMETHODS_H
