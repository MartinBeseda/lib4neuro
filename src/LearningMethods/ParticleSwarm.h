/**
 * DESCRIPTION OF THE FILE
 *
 * @author Michal Kravčenko
 * @date 2.7.18 -
 */

#ifndef INC_4NEURO_PARTICLESWARM_H
#define INC_4NEURO_PARTICLESWARM_H

#include <cstdlib>
#include <ctime>
#include <cmath>
#include <set>
#include <stdexcept>
#include <random>
#include <iterator>
#include <algorithm>

#include "Network/NeuralNetwork.h"
#include "DataSet/DataSet.h"
#include "ErrorFunction/ErrorFunctions.h"


class Particle{
private:

    size_t coordinate_dim;
    std::vector<double> *coordinate = nullptr;
    std::vector<double> *velocity = nullptr;

    std::vector<double> *optimal_coordinate = nullptr;
    double optimal_value;

    double r1;
    double r2;
    double r3;

    double current_val;

    ErrorFunction* ef;

    double *domain_bounds;


    void randomize_coordinates();

    void randomize_velocity();

    void randomize_parameters();

public:

    /**
     *
     */
    void print_coordinate();

    /**
     *
     * @param f_dim
     */
    Particle(ErrorFunction* ef, double *domain_bounds);
    ~Particle( );

    /**
     *
     * @return
     */
    std::vector<double>* get_coordinate();

    /**
     *
     * @return
     */
    double get_current_value();

    /**
     *
     * @return
     */
    double get_optimal_value();

    /**
     *
     * @param ref_coordinate
     */
    void get_optimal_coordinate(std::vector<double> &ref_coordinate);

    /**
     *
     * @param w
     * @param c1
     * @param c2
     * @param glob_min_coord
     * @param penalty_coef
     */
    double change_coordinate(double w, double c1, double c2, std::vector<double> &glob_min_coord, std::vector<std::vector<double>> &global_min_vec, double penalty_coef=0.25);
};


class ParticleSwarm {

private:

    /**
     *
     */
    Particle** particle_swarm = nullptr;

    /**
     *
     */
    ErrorFunction* f;

    size_t func_dim;

    size_t n_particles;

    size_t iter_max;

    double c1;

    double c2;

    double c3;

    double w;

    double global_optimal_value;

    double *domain_bounds;

    std::vector<double> *p_min_glob = nullptr;

protected:
    /**
     *
     * @param coord
     * @param val
     * @return
     */
    Particle* determine_optimal_coordinate_and_value(std::vector<double> &coord, double &val);

    /**
     *
     * @return
     */
    std::vector<double>* get_centroid_coordinates();

    /**
     *
     * @param a
     * @param b
     * @param n
     * @return
     */
    double get_euclidean_distance(std::vector<double>* a, std::vector<double>* b);

public:

    /**
     *
     * @param ef
     * @param f_dim
     * @param domain_bounds
     * @param c1
     * @param c2
     * @param w
     * @param n_particles
     * @param iter_max
     */
     //TODO make domain_bounds constant
    ParticleSwarm( ErrorFunction* ef, std::vector<double> *domain_bounds, double c1 = 1.711897, double c2 = 1.711897, double w = 0.711897, size_t n_particles = 50, size_t iter_max = 1000 );

    /**
     *
     */
    ~ParticleSwarm( );


    /**
     *
     * @param gamma
     * @param epsilon
     * @param delta
     */
    void optimize( double gamma, double epsilon, double delta=0.7 );

    /**
     *
     * @return
     */
    std::vector<double>* get_solution();


};


#endif //INC_4NEURO_PARTICLESWARM_H
