#!/bin/bash

##############
# UNIT TESTS #
##############
for f in build/bin/unit-tests/*_test; do
    ${f} || exit -1
done
