@echo off

rmdir /s /q build 2>NUL
del Makefile 2>NUL
del /s /q "docs/*" 2>NUL
del "src/*TestRunner*" 2>NUL
del "src/*.o src/*.mod" 2>NUL
del src/funit.tmp 2>NUL
del "src/*_fun.f90" 2>NUL
del CMakeCache.txt 2>NUL
del cmake_install.cmake 2>NUL
del src/cmake_install.cmake 2>NUL
del /s /q CMakeFiles 2>NUL
del "*.vcxproj" 2>NUL
del "*.vcxproj.filters" 2>NUL
del "*.sln" 2>NUL
rmdir /s /q CMakeFiles 2>NUL   
rmdir /s /q src/CMakeFiles 2>NUL
rmdir /s /q src/examples/CMakeFiles 2>NUL
rmdir /s /q src/tests/CMakeFiles 2>NUL
