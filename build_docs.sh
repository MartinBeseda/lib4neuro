#!/bin/bash

echo "Generating documentation into folder 'docs'...";
doxygen 2>&1;
cd docs/latex;
echo "Building LaTeX documentation../";
make >/dev/null && echo "Documentation was built." || exit -1;
cd ../..;
